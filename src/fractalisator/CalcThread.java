package fractalisator;

import library.Complex;
import library.Data;
import library.SyncBufferedImage;
import library.Vec;


public class CalcThread extends Thread{
	
	private int nb;
	private boolean running;
	private SyncBufferedImage img;
	private Data data;
	
	private double smooth_divisor;
		
	public CalcThread(int nb, SyncBufferedImage img, Data data){
		this.nb = nb;
		running = false;
		this.img = img;
		this.data = data;
		this.smooth_divisor = Math.abs(data.power.toDouble())<2 ? 2 : Math.log(Math.abs(data.power.toDouble()));
	}
	
	public void setData(Data data){
		this.data = data;
		this.smooth_divisor = Math.abs(data.power.toDouble())<2 ? 2 : Math.log(Math.abs(data.power.toDouble()));
	}
			
	public void run(){
		if(this.img==null || this.data==null) return;
		System.out.println("Thread "+nb+" running.");
		running = true;
		
		Vec next;
		while((next = img.getNext())!=null && running){
			int x = next.getX(), y = next.getY(), z = next.getZ();
			
			if(z==0)
				calcES(x,y);
			else if(z==1)
				img.setRGB(x, y, data.getRGB(img.getES(x,y)));
		}
		
		running = false;
		System.out.println("Thread "+nb+" shut down.");
	}
	
	private void calcES(int x, int y){
		Complex
		c = data.julia_s ? data.julia : new Complex(),
		z = new Complex(),
		mod = data.julia_s ? z : c;
	
		//c = (a+stepsize*x + (b+stepsize*y)i
		mod.setReal(data.coords.getC().getReal()+data.coords.step()*x);
		mod.setImag(data.coords.getC().getImag()+data.coords.step()*y);

		//escapespeed := smallest n of z_n(c) with |z_n(c)| > 2.
		//z0=0, z_(n+1) = (z_n)^2 + c
		int es = 1;
		if(!data.julia_s) z.copy_inp(c);
		while(z.abs_sqr()<4 && es<data.max_iter && running){
			switch(data.fractal){
				case Mandelbrot :
					z.pow_f_inp(data.power).add_inp(c); break;
				case BurningShip :
					z.abs_sep().pow_f_inp(data.power).add_inp(c); break;
				case Mandelbar :
					z.conj_inp().pow_f_inp(data.power).add_inp(c); break;
				case Pokorny :
					z.pow_f_inp(data.power).add_inp(c).div_op_inp(Complex.C_ONE_IMAG); break;
				case BurningPokorny :
					z.abs_sep().pow_f_inp(data.power).add_inp(c).div_op_inp(Complex.C_ONE_REAL); break;
				case Test :
					z.pow_f_inp(data.power).add_inp(c.mult(2)); break;
				case PhoenixDEC :
					z.add_inp(new Complex(c.getReal(),0)).add_inp(new Complex(0,c.getImag())).pow_f_inp(data.power).add_inp(c); break;
				default :
					es = data.max_iter;
			}	
			es++;
		}
		
		if(data.cp.isSmooth())
			es += 1 - Math.log(Math.log(z.abs())) / smooth_divisor;
		data.setESMinMax(es);
		img.setES(x, y, es);
	}
	
	public void shutdown(){
		running = false;
	}
}
