package fractalisator;


import java.awt.image.BufferedImage;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import library.ColorPattern;
import library.ColorRel;
import library.ColorType;
import library.Complex;
import library.Coords;
import library.Data;
import library.Frac;
import library.Fractal;
import library.Settings;
import library.SyncBufferedImage;
import library.Vec;


public class Task{
	
	private static Settings set;
	
	private static SyncBufferedImage image;
	
	private static int cores;
	
	private static CalcThread[] threads;
	
	public static final int version = 14;
	
	public static void main(String[] args){
			
		resetSettings();
		
		image = new SyncBufferedImage(set.getSize(), BufferedImage.TYPE_INT_RGB);
		
		cores = Runtime.getRuntime().availableProcessors();
		if(cores<1) cores = 1;
		System.out.println(cores+" cores available.");
		
		threads = new CalcThread[cores];
		for(int i=0;i<cores;i++){
			threads[i] = new CalcThread(i+1, image, set.getCurrentData());
			threads[i].start();
		}
		
		String path = "C:\\Users\\Morrow\\Desktop\\Mandelbrot -5000to7000";
		
		int scale = 250, from = -5*scale, to = 7*scale;
		for(int i=from;i<=to;i++){
			System.out.println(i+" / "+from+" to "+to);
			set.getCurrentData().power = new Frac(i,scale);
			calculate();
			saveImg(path, i);
		}
	
	}
	
	private static Settings resetSettings(){
		Vec size = new Vec(1080,1080);
		Coords coords = new Coords(size);
		ColorPattern cp = new ColorPattern(
				ColorType.linear_halfpeak,
				ColorRel.orange,
				true
		);
		Data data = new Data(coords,
				Fractal.Mandelbrot,
				new Frac(2),
				false,
				new Complex(-0.8,-0.2), 
				50,
		cp);
		return new Settings(version, size, 10, data);
	}
	
	public static void redraw(){
		zoom(set.getSize().div(2), -1);
	}
	
	public static void zoom(Vec click, int zoomvar){
		set.getCurrentData().coords.zoom(set.getSize(), click, zoomvar, set.zoom);
	}
	
	private static BufferedImage calculate(){
		System.out.print("Rendering image... ");
		long t = System.currentTimeMillis();
		
		for(int i=0;i<threads.length;i++)
			threads[i].setData(set.getCurrentData());
				
		if(set.getSize().equals(image.getSize()))
			image.reset();
		else
			image.setSize(set.getSize());
		
		if(!image.waiting())
			return new BufferedImage(set.getSize().getX(), set.getSize().getY(), BufferedImage.TYPE_INT_RGB);
				
		String status2 = "in "+(System.currentTimeMillis()-t)+"ms";
		System.out.println(status2);
		return image.toBufferedImage();
	}		
	
	static void saveImg(String path, int nb){
		try {
		    File outputfile = new File(path+"\\screen "+System.currentTimeMillis()+" "+nb+".png");
		    ImageIO.write(image.toBufferedImage(), "png", outputfile);
		} catch (IOException e) {
		    System.out.println("Couldn't create screenshot.");
		}
	}
	
}