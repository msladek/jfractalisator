package fractalisator;

import gui.GUI;

import java.awt.Desktop;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.imageio.ImageIO;

import library.ColorPattern;
import library.ColorRel;
import library.ColorType;
import library.Complex;
import library.Coords;
import library.Data;
import library.Frac;
import library.Fractal;
import library.Settings;
import library.SyncBufferedImage;
import library.Vec;


/* 
 * TODO
 * - phoenix
 * - gui center setting possibility
 * - new coloring (Mandelbrot)
 * - organize gui (not load anew every frame!)
 */

public class FractalGenerator{
	
	public static final int version = 19;
	
	private static GUI gui;
	private static Settings set;
	
	private static SyncBufferedImage image;
	
	private static int cores;
	private static String path;
	
	private static CalcThread[] threads;
	
	public static void main(String[] args){
		System.out.println("Java Fractalisator");
		System.out.println("� by Marc Sladek, 2012, Switzerland");
		System.out.println("http://www.java-fractalisator.ch.vu");
		System.out.println("");
		System.out.println("Startup...");
		try{ path = System.getProperty("user.home")+File.separator+".javaFractalisator"+File.separator;
		}catch(Exception e){ path = ""; }
		System.out.println("Settings path: "+path);
		
		set = loadSettings();
				
		image = new SyncBufferedImage(set.getSize(), BufferedImage.TYPE_INT_RGB);
		
		cores = Runtime.getRuntime().availableProcessors();
		if(cores<1) cores = 1;
		System.out.println(cores+" cores available.");
		
		threads = new CalcThread[cores];
		for(int i=0;i<cores;i++){
			threads[i] = new CalcThread(i+1, image, set.getCurrentData());
			threads[i].start();
		}
		
		gui = new GUI(image.toBufferedImage(), cores);
		System.out.println("GUI initialised.");
		System.out.println("...started up.");
		System.out.println("");
		
		redraw();
		
	}
	
	private static Settings resetSettings(){
		Vec size = new Vec((int)Toolkit.getDefaultToolkit().getScreenSize().getWidth()-500 , (int)Toolkit.getDefaultToolkit().getScreenSize().getHeight()-100);
		if(size.getX()<600) size = new Vec(600, size.getY());
		if(size.getY()<600) size = new Vec(size.getX(), 600);
		Coords coords = new Coords(size);
		ColorPattern cp = new ColorPattern(
				ColorType.linear_halfpeak,
				ColorRel.orange,
				true
		);
		Data data = new Data(coords,
				Fractal.Mandelbrot,
				new Frac(2),
				false,
				new Complex(-0.8,-0.2), 
				100,
		cp);
		return new Settings(version, size, 10, data);
	}
	
	public static void redraw(){
		gui.setImg(calculate(), set.getCurrentData().coords);
	}
		
	public static void redraw(Data data){
		set.setData(data);
		redraw();
	}
	
	public static void zoom(Vec click, int zoomvar){
		Data newData = set.getCurrentData().copy();
		newData.coords.zoom(set.getSize(), click, zoomvar, set.zoom);
		set.setData(newData);
		redraw();
	}
	
	private static BufferedImage calculate(){
		String status = ("Rendering image... ");
		System.out.print(status);
		gui.setStatus(status);
		long t = System.currentTimeMillis();
		
		for(int i=0;i<threads.length;i++)
			threads[i].setData(set.getCurrentData());
				
		if(!set.getSize().equals(image.getSize()))
			image.setSize(set.getSize());
		image.reset();
		
		if(!image.waiting())
			return new BufferedImage(set.getSize().getX(), set.getSize().getY(), BufferedImage.TYPE_INT_RGB);
		
		status = "Done in "+(System.currentTimeMillis()-t)/1000.0+"s";
		System.out.println(status);
		gui.setStatus(status);
		return image.toBufferedImage();
	}
					
	public static Vec getSize(){ return set.getSize(); }
	public static int getZoom(){ return set.zoom; }
	public static Data getData(){ return set.getCurrentData(); }
	public static boolean hasLastData(){ return set.hasLastData();	}
	public static Data[] getSaves(){ return set.getSaves(); }
	
	private static void setData(Data data){
		set.setData(data);
	}
		
	public static void setLastToCurrentData(){
		set.setLastToCurrentData();
		redraw();
	}
	
	public static void save(){
		set.save();
	}
	public static void delete(Data data){
		set.delete(data);
	}
	
	
	public static void setParam(boolean redraw, Vec new_size, int zoom, Data data){	
		set.setSize(new_size);
		set.zoom = zoom;
		
		if(set.getCurrentData().needCoordsReset(data))
			data.coords = new Coords(set.getSize());
			
		if(data.julia_s)
			gui.setMode(true);
		
		if(redraw)
			redraw(data);
		else
			setData(data);
	}
	
	public static void reset(){
		Data newData = set.getCurrentData().copy();
		newData.coords = new Coords(set.getSize());
		redraw(newData);
	}
	
	public static void setJulia(Vec vec){
		Data data = set.getCurrentData();
		data.julia = data.coords.transform(vec);
		gui.setJuliaC(data.julia);
	}
					
	public static void saveImg(){
		try {
		    File file = new File(path+"screenshots"+File.separator+"screen "+System.currentTimeMillis()/1000+".png");
		    file.getParentFile().mkdirs();
		    ImageIO.write(image.toBufferedImage(), "png", file);
		    System.out.println("Screenshot created.");
		} catch (IOException e) {
		    System.out.println("Couldn't create screenshot.");
		}
	}
	
	public static void openFolder(){
		Desktop desktop = null;
		if(Desktop.isDesktopSupported())
			desktop = Desktop.getDesktop();
		else
			return;
		try {
			desktop.open(new File(path));
		} catch(IOException e){}
	}
	
	private static Settings loadSettings(){
		ObjectInputStream ois = null;
		Settings ret = null;
		
		try {
		    FileInputStream fis = new FileInputStream(path+"settings");
    		BufferedInputStream bis = new BufferedInputStream(fis);
		    ois = new ObjectInputStream(bis);
		    ret = (Settings)ois.readObject();
		    if(ret.version!=version) throw new Exception();
		    System.out.println("Settings loaded.");
		} catch(Exception e){
			System.err.println("Couldn't load settings.");
			ret = resetSettings();
		}
		finally{ try{ ois.close(); } catch(Exception e){} }
				
		File dir = new File(path+"saves");
		dir.mkdirs();
		new File(path+"screenshots").mkdirs();
				
		FileFilter filter = new FileFilter(){
			public boolean accept(File file) { 
				return file.getName().contains(".jfrac"); 
			}
		}; 
		File[] files = dir.listFiles(filter);
		Data[] datas = new Data[0];
		if(files!=null){
			datas = new Data[files.length];
			for(int i=0;i<files.length;i++) try {
				FileInputStream fis = new FileInputStream(files[i]);
				BufferedInputStream bis = new BufferedInputStream(fis);
				ois = new ObjectInputStream(bis);
				datas[i] = (Data)ois.readObject();
				System.out.println("Loaded: "+files[i].getName());
			} catch(Exception e){ System.err.println("Couldn't load: "+files[i].getName()); }
			finally{ try{ ois.close(); } catch(Exception e){} }
		}
		ret.setSaves(datas);
		return ret;
	}
	
	private static void saveSettings(){
		Data[] datas = set.getSaves();
		
		File dir = new File(path+"saves");
		deleteDir(dir);
		dir.mkdirs();
		
		String name = "";
		for(Data data : datas)
			try{
				name = data.toString()+".jfrac";
				saveObject(path+"saves"+File.separator+name , data);
				System.out.println("Saved: "+name);
			} catch(Exception e){
				System.err.println("Couldn't save "+name);
			}

		try{
		    saveObject(path+"settings", set);
			System.out.println("Settings saved.");
		} catch(Exception e){ System.err.println("Couldn't save settings."); }
	}
	
	private static void saveObject(String path, Object o) throws IOException{
		FileOutputStream fos = new FileOutputStream(path);
		BufferedOutputStream bos = new BufferedOutputStream(fos);
		ObjectOutputStream oos  = new ObjectOutputStream(bos);
		oos.writeObject(o);
		oos.close();
	}
	
	private static boolean deleteDir(File dir) {
	    if(dir.isDirectory())
	        for(String child : dir.list())
	            if(!deleteDir(new File(dir, child)))
	                return false;
	    return dir.delete();
	}
	
	public static void shutdown(){
		System.out.println("");
		System.out.println("Shutdown initialised...");
		for(int i=0;i<threads.length;i++)
			threads[i].shutdown();
		image.reset();
		saveSettings();
		try{ Thread.sleep(1000); } catch(InterruptedException e){}
		System.out.println("...shutted down.");
		System.exit(0);
	}
}