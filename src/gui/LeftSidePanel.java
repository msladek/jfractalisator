package gui;

import java.awt.FlowLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

import fractalisator.FractalGenerator;

import library.ColorPattern;
import library.ColorRel;
import library.ColorType;
import library.Complex;
import library.Data;
import library.Frac;
import library.Fractal;
import library.Vec;


public class LeftSidePanel extends JPanel{
	private static final long serialVersionUID = 1L;
	
	JButton resetButton;
	JCheckBox
		juliaBox,
		smoothBox;
	JComboBox<Fractal> fractalBox;
	JComboBox<ColorType> typeBox;
	JComboBox<ColorRel>	colorBox;
	JTextField
		resoWField,
		resoHField,
		zoomField,
		powerNumField,
		powerDenField,
		julreField,
		julimField,
		iterField;

	LeftSidePanel(){
		super();
		initialise();
	}
					
	private void initialise(){
		setBorder(new BevelBorder(BevelBorder.RAISED));
		JPanel mainPanel = new JPanel(), panel, groupPanel;
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		
			AL al = new AL();
			KL kl = new KL();
			Data data = FractalGenerator.getData();
	
			panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
				panel.add(new JLabel("Resolution"));
			mainPanel.add(panel);
			panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
			Vec size = FractalGenerator.getSize();
			resoWField = new JTextField();
			resoWField.setColumns(4);
			resoWField.setText(""+size.getX());
			resoWField.setEditable(true);
			resoWField.addKeyListener(kl);
			panel.add(resoWField);
			panel.add(new JLabel("x"));
			resoHField = new JTextField();
			resoHField.setColumns(4);
			resoHField.setText(""+size.getY());
			resoHField.setEditable(true);
			resoHField.addKeyListener(kl);
			panel.add(resoHField);
			mainPanel.add(panel);
		
			panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
				panel.add(new JLabel("Zoom"));
			mainPanel.add(panel);
			zoomField = new JTextField();
			zoomField.setText(""+FractalGenerator.getZoom());
			zoomField.setEditable(true);
			zoomField.addKeyListener(new KL());
			mainPanel.add(zoomField);
			
			mainPanel.add(Box.createVerticalStrut(10));
			panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
			resetButton = new JButton("Reset Zoom");
			resetButton.addActionListener(new ResetAL());
			panel.add(resetButton);
			mainPanel.add(panel);
		
		mainPanel.add(Box.createVerticalStrut(10));
		mainPanel.add(new JSeparator(SwingConstants.HORIZONTAL));
		mainPanel.add(Box.createVerticalStrut(10));
			
			panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
				panel.add(new JLabel("Fractal"));
			mainPanel.add(panel);
			fractalBox = new JComboBox<Fractal>(Fractal.values());
			fractalBox.setSelectedItem(data.fractal);
			fractalBox.addActionListener(al);
			mainPanel.add(fractalBox);
			
			panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
				panel.add(new JLabel("Power"));
			mainPanel.add(panel);
			panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
			Frac pow = data.power;
			powerNumField = new JTextField();
			powerNumField.setColumns(4);
			powerNumField.setText(""+pow.getNum());
			powerNumField.setEditable(true);
			powerNumField.addKeyListener(kl);
			panel.add(powerNumField);
			panel.add(new JLabel("/"));
			powerDenField = new JTextField();
			powerDenField.setColumns(4);
			powerDenField.setText(""+pow.getDen());
			powerDenField.setEditable(true);
			powerDenField.addKeyListener(kl);
			panel.add(powerDenField);
			mainPanel.add(panel);
			
			mainPanel.add(Box.createVerticalStrut(5));
			
			groupPanel = new JPanel();
			groupPanel.setLayout(new BoxLayout(groupPanel, BoxLayout.Y_AXIS));
			groupPanel.setBorder(new BevelBorder(BevelBorder.LOWERED));
				panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
					juliaBox = new JCheckBox("Julia set");
					juliaBox.setSelected(data.julia_s);
					juliaBox.addActionListener(al);
				panel.add(juliaBox);
				groupPanel.add(panel);
				Complex julia = data.julia;
				julreField = new JTextField();
				julreField.setText(Double.toString(julia.getReal()));
				julreField.addKeyListener(kl);
				groupPanel.add(julreField);
				julimField = new JTextField();
				julimField.setText(Double.toString(julia.getImag()));
				julimField.addKeyListener(kl);
				groupPanel.add(julimField);
				setJuliaFields();
			mainPanel.add(groupPanel);
		
		mainPanel.add(Box.createVerticalStrut(20));
		mainPanel.add(new JSeparator(SwingConstants.HORIZONTAL));
		mainPanel.add(Box.createVerticalStrut(10));
		
			panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
			panel.add(new JLabel("Coloring"));
			mainPanel.add(panel);
			ColorPattern cp = data.cp;
			typeBox = new JComboBox<ColorType>(ColorType.values());
			typeBox.setSelectedItem(cp.getType());
			typeBox.addActionListener(al);
			mainPanel.add(typeBox);
			colorBox = new JComboBox<ColorRel>(ColorRel.values());
			colorBox.setSelectedItem(cp.getColor());
			colorBox.addActionListener(al);
			mainPanel.add(colorBox);
			panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
				smoothBox = new JCheckBox("Smooth");
				smoothBox.setSelected(cp.isSmooth());
				smoothBox.addActionListener(al);
				panel.add(smoothBox);
			mainPanel.add(panel);
		
		mainPanel.add(Box.createVerticalStrut(20));
		mainPanel.add(new JSeparator(SwingConstants.HORIZONTAL));
		mainPanel.add(Box.createVerticalStrut(10));
		
			panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
			panel.add(new JLabel("Max Iteration"));
			mainPanel.add(panel);
			iterField = new JTextField();
			iterField.setText(""+data.max_iter);
			iterField.setEditable(true);
			iterField.addKeyListener(kl);
			mainPanel.add(iterField);
				
		add(mainPanel);
	}
	
	private void setJuliaFields(){
		boolean editable = juliaBox.isSelected();
		julreField.setEditable(editable);
		julimField.setEditable(editable);
	}
	    
    void setAllParam(boolean redraw){
    	
    	Data data = FractalGenerator.getData();
    	    	    	
		Vec new_size;
		int
			zoom,
			iter;
		Frac power;
		Fractal fractal = (Fractal)fractalBox.getSelectedItem();
		boolean
			julia_s = juliaBox.isSelected();
		Complex julia;
		ColorPattern cp = new ColorPattern((ColorType)typeBox.getSelectedItem(), (ColorRel)colorBox.getSelectedItem(), smoothBox.isSelected());
		
    	try{
    		new_size = new Vec(new Integer(resoWField.getText()), new Integer(resoHField.getText()));
    	}catch(NumberFormatException e){
			new_size = FractalGenerator.getSize();
			resoWField.setText(""+new_size.getX());
			resoHField.setText(""+new_size.getY());
    	}
    	
    	try{
			iter = new Integer(iterField.getText());
			if(iter<0) throw new NumberFormatException();
    	}catch(NumberFormatException e){
    		iter = data.max_iter;
    		iterField.setText(""+iter);
    	}
    	
    	try{
			zoom = new Integer(zoomField.getText());
			if(zoom<0) throw new NumberFormatException();
    	}catch(NumberFormatException e){
    		zoom = FractalGenerator.getZoom();
    		zoomField.setText(""+zoom);
    	}
    	
    	try{
    		power = new Frac(new Integer(powerNumField.getText()), new Integer(powerDenField.getText()));
    	}catch(NumberFormatException e){
			power = data.power;
			powerNumField.setText(""+power.getNum());
			powerDenField.setText(""+power.getDen());
    	}
    	
    	try{
    		julia = new Complex(new Double(julreField.getText()), new Double(julimField.getText()));
    	}catch(NumberFormatException e){
			julia = data.julia;
			julreField.setText(""+julia.getReal());
			julimField.setText(""+julia.getImag());
    	}
    	    	
    	data = new Data(
    		data.coords,
    		fractal, power,
    		julia_s, julia,
    		iter,
    		cp
    	);
    	
    	FractalGenerator.setParam(
    		redraw,
    		new_size,
    		zoom,
    		data
    	);
    }
    
	
	private class AL implements ActionListener{
		public void actionPerformed(ActionEvent e){
			setJuliaFields();
			setAllParam(true);
		}
    }
    
    private class ResetAL implements ActionListener{
		public void actionPerformed(ActionEvent e){
			FractalGenerator.reset();
		}
    }
        
    private class KL implements KeyListener{
    	public void keyTyped(KeyEvent e) {
        	char c = e.getKeyChar();
        	if(c == KeyEvent.VK_ENTER)
        		setAllParam(true);
        	else if( !( 
        			Character.isDigit(c) ||
        			c == KeyEvent.VK_BACK_SPACE ||
        			c == KeyEvent.VK_DELETE ||
        			c == KeyEvent.VK_MINUS ||
        			c == KeyEvent.VK_PERIOD)
        	){
        		getToolkit().beep();
        		e.consume();
        	}
    	}
        public void keyReleased(KeyEvent e) {}
        public void keyPressed(KeyEvent e) {}
    }
}
