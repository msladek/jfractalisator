package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;
import javax.swing.border.BevelBorder;


public class StatusPanel extends JPanel{
	private static final long serialVersionUID = 1L;
	
	private int cores = 1;
	private String status;
		
	StatusPanel(int cores){
		super();
		this.cores = cores;
		this.status = "";
		initialise();
	}
					
	private void initialise(){
		this.setBackground(Color.white);
		this.setLayout(null);
		this.setBorder(new BevelBorder(BevelBorder.LOWERED));
		this.setPreferredSize(new Dimension((int)this.getPreferredSize().getWidth(), 20));
	}
	
	public void paintComponent(Graphics g){
		
		super.paintComponent(g);
		g.drawString("Core usage: "+cores+" thread"+(cores>1?"s":""),5,15);
		g.drawString(status,(int)this.getSize().getWidth()-160,15);
	}
	
	void setStatus(String status){
		this.status = status;
		this.invalidate();
		this.repaint();
	}
}
