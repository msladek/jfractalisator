package gui;

import java.awt.Dimension;
import java.awt.FlowLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.BevelBorder;

import fractalisator.FractalGenerator;

import library.Data;

public class RightSidePanel extends JPanel{
	private static final long serialVersionUID = 1L;
		
	JButton
		screenButton,
		openButton,
		backButton,
		loadButton,
		saveButton,
		deleteButton;
	JList<Data> saveList;
	
	RightSidePanel(){
		super();
		initialise();
	}
					
	private void initialise(){
		setBorder(new BevelBorder(BevelBorder.RAISED));
		JPanel mainPanel = new JPanel(), panel;
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		mainPanel.setMaximumSize(mainPanel.getPreferredSize());
			
			panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
				screenButton = new JButton("Screenshot");
				screenButton.addActionListener(new ScreenAL());
				screenButton.setEnabled(true);
			panel.add(screenButton);
			mainPanel.add(panel);
			panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
				openButton = new JButton("Open Folder");
				openButton.addActionListener(new OpenAL());
				openButton.setEnabled(true);
				panel.add(openButton);
			mainPanel.add(panel);
		
		
		mainPanel.add(Box.createVerticalStrut(10));
		mainPanel.add(new JSeparator(SwingConstants.HORIZONTAL));
		mainPanel.add(Box.createVerticalStrut(10));
		
        //set account and active list
			panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
				backButton = new JButton("Back");
				backButton.addActionListener(new backAL());
				backButton.setEnabled(true);
				loadButton = new JButton("Load");
				loadButton.addActionListener(new loadAL());
				loadButton.setEnabled(true);
			panel.add(backButton);
			panel.add(loadButton);
			mainPanel.add(panel);
			saveList = new JList<Data>(FractalGenerator.getSaves());
			JScrollPane saveScrollPane = new JScrollPane(saveList);
			saveScrollPane.setPreferredSize(new Dimension(mainPanel.getPreferredSize().width,mainPanel.getPreferredSize().height*2));
			mainPanel.add(saveScrollPane);
			panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
				saveButton = new JButton("Save");
				saveButton.addActionListener(new saveAL());
				saveButton.setEnabled(true);
				deleteButton =  new JButton("Delete");
				deleteButton.addActionListener(new deleteAL());
				deleteButton.setEnabled(true);
			panel.add(saveButton);
			panel.add(deleteButton);
			mainPanel.add(panel);
			
			setButtons();
				
		add(mainPanel);
	}
	
	private void setButtons(){
		if(saveList.getModel().getSize()==0){
			deleteButton.setEnabled(false);
			loadButton.setEnabled(false);
		}
		else{
			deleteButton.setEnabled(true);
			loadButton.setEnabled(true);
		}
		if(FractalGenerator.hasLastData())
			backButton.setEnabled(true);
		else
			backButton.setEnabled(false);
	}
	
	private class backAL implements ActionListener{
		public void actionPerformed(ActionEvent e){
			try { SwingUtilities.invokeLater(new Runnable(){ public void run(){
				FractalGenerator.setLastToCurrentData();
	        }});} catch(Exception ex){
	        	System.err.println(ex);
	        }
		}
	}
	
	private class loadAL implements ActionListener{
		public void actionPerformed(ActionEvent e){
			try { SwingUtilities.invokeLater(new Runnable(){ public void run(){
				Data data = saveList.getSelectedValue();
				if(data!=null) FractalGenerator.redraw(data);
	        }});} catch(Exception ex){
	        	System.err.println(ex);
	        }
		}
	}
	
	private class saveAL implements ActionListener{
		public void actionPerformed(ActionEvent e){
			try { SwingUtilities.invokeLater(new Runnable(){ public void run(){
				FractalGenerator.save();
				saveList.setListData(FractalGenerator.getSaves());
				setButtons();
			}});} catch(Exception ex){
				System.err.println(ex);
			}
		}
	}
	    
	private class deleteAL implements ActionListener{
		public void actionPerformed(ActionEvent e){
			try { SwingUtilities.invokeLater(new Runnable(){ public void run(){
				@SuppressWarnings("deprecation") /* getSelectedValuesList() not working in ,jar file! */
				Object[] datas = saveList.getSelectedValues();
				for(Object data : datas)
					FractalGenerator.delete((Data)data);
				saveList.setListData(FractalGenerator.getSaves());
				setButtons();
			}});} catch(Exception ex){
				System.err.println(ex);
			}
		}
	}
	    
    private class ScreenAL implements ActionListener{
		public void actionPerformed(ActionEvent e){
			FractalGenerator.saveImg();
		}
    }
    
    private class OpenAL implements ActionListener{
		public void actionPerformed(ActionEvent e){
			FractalGenerator.openFolder();
		}
    }
}
