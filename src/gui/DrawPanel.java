package gui;
import java.awt.Color;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import fractalisator.FractalGenerator;

import library.Coords;
import library.Vec;


public class DrawPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	
	private BufferedImage img;
	private String mode, coord1, coord2;
	private boolean zoom;
		
	DrawPanel(BufferedImage img, boolean zoom, Coords coords){
		super();
		this.img = img;
		this.coord1 = "Pos: "+coords.getC().round(100000);
		this.coord2 = "Zoom: "+Math.round(coords.getZoom()*1000)/1000f+"x";
		this.setMode(zoom);
		initialise();
	}
	
	private void initialise(){
		this.setBackground(Color.white);
		this.setLayout(null);
		this.setPreferredSize(new Dimension(img.getWidth(),img.getHeight()));
		this.addMouseListener(new ClickConverter());
	}
		
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D)g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.drawImage(img, 0, 0, this);
		g2.setColor(Color.white);
		g2.drawString(mode,10,20);
		g2.drawString(coord1,10,40);
		g2.drawString(coord2,10,60);
	}
	
	boolean getMode(){ return this.zoom; }
	
	void setImg(BufferedImage img, Coords coords){
		this.img = img;
		this.coord1 = "Pos: "+coords.getC().round(100000);
		this.coord2 = "Zoom: "+Math.round(coords.getZoom()*1000)/1000f+"x";
		this.setPreferredSize(new Dimension(img.getWidth(),img.getHeight()));
		this.invalidate();
		this.repaint();
	}
		
	void setMode(boolean zoom){
		this.zoom = zoom;
		if(zoom) this.mode = "Zooming mode";
		else this.mode = "Julia C setting mode";
		this.invalidate();
		this.repaint();
	}
            
    private class ClickConverter implements MouseListener{
		public void mouseClicked(MouseEvent e){
			Vec click = new Vec(e.getX(),e.getY());
			
			if(e.getButton()==MouseEvent.BUTTON1)
				if(zoom) FractalGenerator.zoom(click,0);
				else FractalGenerator.setJulia(click);
			else if(e.getButton()==MouseEvent.BUTTON3)
				if(zoom) FractalGenerator.zoom(click,1);
				else FractalGenerator.setJulia(click);
			else if(e.getButton()==MouseEvent.BUTTON2)
				if(zoom) FractalGenerator.zoom(click,2);
				else FractalGenerator.setJulia(click);
		}
		public void mouseEntered(MouseEvent e){}
		public void mouseExited(MouseEvent e){}
		public void mousePressed(MouseEvent e){}
		public void mouseReleased(MouseEvent e){}   	
    }
}
