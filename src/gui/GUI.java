package gui;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import fractalisator.FractalGenerator;

import library.Complex;
import library.Coords;

public class GUI extends JFrame{
	private static final long serialVersionUID = 1L;
	private DrawPanel drawPanel = null;
	private LeftSidePanel lSidePanel = null;
	private RightSidePanel rSidePanel = null;
	private StatusPanel statusPanel = null;
				
	public GUI(BufferedImage img, int cores){
		super();
		initialise(img, cores);
	}
				
	private void initialise(BufferedImage img, int cores){
		
		JPanel panel = new JPanel(new BorderLayout());
		drawPanel = new DrawPanel(img, true, FractalGenerator.getData().coords);
		lSidePanel = new LeftSidePanel();
		rSidePanel = new RightSidePanel();
		statusPanel = new StatusPanel(cores);
		panel.add(drawPanel, BorderLayout.CENTER);
		panel.add(lSidePanel, BorderLayout.WEST);
		panel.add(rSidePanel, BorderLayout.EAST);
		panel.add(statusPanel, BorderLayout.SOUTH);
		
		this.setLocation(0,0);
		this.setTitle("Java Fractalisator");
		this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new WindowAdapter(){ public void windowClosing(WindowEvent e){ closeGUI();}});
		this.setContentPane(panel);
		this.setResizable(true);
		redo();
		
		KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(new MyDispatcher());		
	}
	
	private void redo(){
		this.setSize(this.getPreferredSize());
		this.pack();
		this.invalidate();
		this.setVisible(true);
	}
	
	public void setImg(final BufferedImage img, final Coords coords){
		try { SwingUtilities.invokeLater(new Runnable(){ public void run(){  	
			drawPanel.setImg(img, coords);
        }});} catch(Exception e){
        	System.out.println(e);
        }
		drawPanel.setPreferredSize(new Dimension(img.getWidth(),img.getHeight()));
		redo();
	}
	
	public void setMode(final boolean mode){
		try { SwingUtilities.invokeLater(new Runnable(){ public void run(){  	
			drawPanel.setMode(mode);
        }});} catch(Exception e){
        	System.out.println(e);
        }
	}
		
	public void setJuliaC(final Complex c){
		try { SwingUtilities.invokeLater(new Runnable(){ public void run(){
			Complex c_r = c.round(100000);
			lSidePanel.julreField.setText(""+c_r.getReal());	
			lSidePanel.julimField.setText(""+c_r.getImag());
        }});} catch(Exception e){
        	System.out.println(e);
        }
	}
			
	public void setStatus(final String status){
		try { SwingUtilities.invokeLater(new Runnable(){ public void run(){
			statusPanel.setStatus(status);
			statusPanel.paintImmediately(0, 0, statusPanel.getSize().width, statusPanel.getSize().height);
        }});} catch(Exception e){
        	System.out.println(e);
        }
	}
		
    public void closeGUI(){
    	int confirmed = JOptionPane.showConfirmDialog(this, "Are you sure you want to leave Java Fractalisator?", "Exit", JOptionPane.YES_NO_OPTION); 
        if(confirmed == JOptionPane.YES_OPTION){                 
        	setVisible(false);
        	lSidePanel.setAllParam(false);
        	dispose();
        	FractalGenerator.shutdown();
        }
    }
    					        
    private class MyDispatcher implements KeyEventDispatcher {
        @Override
        public boolean dispatchKeyEvent(KeyEvent e) {
            if (e.getID() == KeyEvent.KEY_PRESSED)
            	if(e.getKeyChar() == KeyEvent.VK_SPACE && !lSidePanel.juliaBox.isSelected()){
            		System.out.println("Mode switched.");
            		setMode(!drawPanel.getMode());
            	}
            return false;
        }
    }
        
}
