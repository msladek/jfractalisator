package test;

import java.util.Random;

import library.Complex;

public class TestSpeed{	
	public static void main(String[] args){
				
		int
			power_from = -167, power_to = -165,
			tests_max = 100,
			calc_max = 1000000;
		
		test(power_from, power_to, tests_max, calc_max);
	}
	
	static void test(int power_from, int power_to, int tests_max, int calc_max){
		Complex[] c1, c2;
		
		for(int power=power_from;power<=power_to;power++){
			System.out.println("Power: "+power);
			
			long total_1 = 0, total_2 = 0;
			
			System.out.print("Test run: ");
			for(int test=0; test<tests_max;test++){
				
				c1 = createRandArray(calc_max);
				c2 = copyArray(c1);
				
				long t = System.currentTimeMillis();
				for(int i=0; i<calc_max;i++)
					c1[i].pow_inp(power);
				total_1 += (System.currentTimeMillis()-t);
			
				t = System.currentTimeMillis();
				for(int i=0; i<calc_max;i++)
					c2[i].root_f_inp(power);
				total_2 += (System.currentTimeMillis()-t);
				
				System.out.print(" "+(test+1));
			}
			
			total_1 /= tests_max;
			total_2 /= tests_max;
			
			System.out.println("");
			System.out.println("Mean Method 1: "+total_1+"ms , Method 2: "+total_2+"ms / 1/2: "+(double)total_1/total_2);
			System.out.println("");
		}
	}
	
	static Complex[] createRandArray(int nb){
		Random rand = new Random();
		Complex[] array = new Complex[nb];
		for(int i=0;i<nb;i++)
			array[i] = new Complex(rand.nextDouble()*255, rand.nextDouble()*255);
		return array;
	}
	
	static Complex[] copyArray(Complex[] c){
		if(c==null) return null;
		Complex[] ret = new Complex[c.length];
		for(int i=0;i<c.length;i++)
				ret[i] = c[i].copy();
		return ret;		
	}
}
