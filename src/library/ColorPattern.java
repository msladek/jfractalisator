package library;

import java.io.Serializable;

public class ColorPattern implements Serializable{
	
	private static final long serialVersionUID = 2702201204181L;

	private ColorType coltype;
	private ColorRel colrel;
	private boolean smooth;
				
	public ColorPattern(ColorType coltype, ColorRel colrel, boolean smooth){
		this.coltype = coltype;
		this.colrel = colrel;
		this.smooth = smooth;
	}
	
	public ColorPattern copy(){ return new ColorPattern(this.coltype, this.colrel, this.smooth); }
	
	public ColorType getType(){ return this.coltype; }
	public ColorRel getColor(){ return this.colrel; }
	public boolean isSmooth(){ return this.smooth; }
	
	int getRGB(double es, double es_min, double es_max, double max, Frac power){		
		int[] rgb = new int[3];
				
		switch(coltype){
		
			case new_linear :
				rgb = new ColorPalette((es-es_min)/(es_max-es_min)).toRGB();
				break;
			
			case new_root :
				rgb = new ColorPalette((Math.sqrt(es)-Math.sqrt(es_min))/(Math.sqrt(es_max)-Math.sqrt(es_min))).toRGB();
				break;
			
			/* y_max = 255: y_max-|y_max/(max/2)*x-y_max| */
			case linear_halfpeak :
				for(int i=0;i<3;i++)
					rgb[i] = (int)Math.round( colrel.get(i) * (255-Math.abs(255/(max/2)*es-255)) );
				break;
				
			case linear_tricolore :
				for(int i=0;i<3;i++)
					rgb[i] =
						es>=max ? 
							0 :
							es<max/3 ? 
								(int)Math.round( colrel.get(i) * 255/(max/3) * es ) :
								es<max*2/3 ?
										(int)Math.round( (colrel.get(i)+1)%2 * 255/(max/3) * (es-max/3) + colrel.get(i) * (255 - 255/(max/3) * (es-max/3)) ) :
										(int)Math.round( (colrel.get(i)+1)%2 * (255 - 255/(max/3) * (es-max*2/3)))
					;
				break;
				
			case linear :
				for(int i=0;i<3;i++)
					rgb[i] = es==max ? 0 : (int)Math.round( colrel.get(i) * 255/max * es );
				break;
				
			case border :
				for(int i=0;i<3;i++)
					rgb[i] =
						es>=max ? 
							0 :
							es<max/2 ? 
								(int)Math.round( colrel.get(i) * 255/(max/2) * es ) : 
								(int)Math.round( (colrel.get(i)+1)%2 * 255/(max/2) * (es-max/2) + colrel.get(i)*255 )
					;
				break;
		
			case border_white :
				for(int i=0;i<3;i++)
					rgb[i] =
						es>=max ? 
							0 :
							(es<max/2) ? 
								(int)Math.round( 255 - (colrel.get(i)+1)%2 * 255/(max/2) * es ) : 
								(int)Math.round( 255 - colrel.get(i) * 255/(max/2) * (es-max/2) - (colrel.get(i)+1)%2*255 )
					;
				break;
		
			case logarithmic :
				for(int i=0;i<3;i++)
					rgb[i] = (int)Math.round(colrel.get(i) * 255/Math.log(max) * Math.log(es));
				break;
				
			case exponential :
				for(int i=0;i<3;i++)
					rgb[i] = (int)Math.round(colrel.get(i) * 255/Math.sqrt(max) * Math.sqrt(es));
				break;
			
		}
					
		return ((rgb[0] << 16) & 0x00FF0000) + ((rgb[1] << 8) & 0x0000FF00) + (rgb[2] & 0x000000FF);
	}
	
}
