package library;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.Stack;


public class Settings implements Serializable{
	
	private static final long serialVersionUID = 2802201206471L;
	
	public int version;
	private Vec size;
	public int zoom;
	private Data currentData;
	private Stack<Data> lastDatas;
	private LinkedList<Data> saves;
	
	public Settings(int version, Vec size, int zoom, Data currentData){
		this.version = version;
		this.size = size;
		this.zoom = zoom;
		this.currentData = currentData;
		this.lastDatas = new Stack<Data>();
		this.saves = new LinkedList<Data>();
	}
	
	public Vec getSize(){ return this.size.copy(); }
	public int getZoom(){ return this.zoom; }
	public Data getCurrentData(){ return this.currentData; }
	public boolean hasLastData(){ return this.lastDatas.isEmpty() ? false : true;	}
	public Data[] getSaves(){ return saves.toArray(new Data[saves.size()]); }
	
	public void setSize(Vec new_size){
		if(!this.size.equals(new_size))
			currentData.coords.newSize(this.size, new_size);
		this.size = new_size;
	}
	
	public void setZoom(int zoom){
		this.zoom = zoom;
	}
	
	public void setData(Data data){
		if(this.lastDatas.size()<=20)
			this.lastDatas.push(currentData.copy());
		this.currentData = data;
	}
	
	public void setLastToCurrentData(){
		if(!this.lastDatas.isEmpty())
			this.currentData = this.lastDatas.pop();
	}
	
	public void save(){
		this.saves.add(currentData.copy());
	}
	
	public void delete(Data data){
		this.saves.remove(data);
	}
	
	public void setSaves(Data[] saves){
		this.saves = new LinkedList<Data>();
		for(Data data : saves)
			this.saves.add(data);
	}
	
}
