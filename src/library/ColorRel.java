package library;

public enum ColorRel{
	
	black		( 0,	0,		0 ),
	white		( 1,	1,		1 ),
	red			( 1,	0,		0 ),
	green		( 0,	1,		0 ),
	blue		( 0,	0,		1 ),
	yellow		( 1,	1,		0 ),
	magenta		( 1,	0,		1 ),
	cyan		( 0,	1,		1 ),
	orange		( 1,	0.5f,	0 ),
	pink		( 1,	0,		0.5f ),
	lime		( 0.5f,	1,		0 ),
	violet		( 0.5f,	0,		1 ),
	bluelight	( 0,	0.5f,	1 );
	
	private final float[] rgb;
	
	private ColorRel(float r, float g, float b){
		this.rgb = new float[] {r,g,b};
	}
	
	public float getR(){ return this.rgb[0]; }
	public float getG(){ return this.rgb[1]; }
	public float getB(){ return this.rgb[2]; }
	public float get(int i) throws IndexOutOfBoundsException { return this.rgb[i]; }
	
	
}
