package library;
import java.awt.Point;
import java.awt.Rectangle;
import java.io.Serializable;

public class Vec extends Object implements Serializable{
	
	private static final long serialVersionUID = 10720112143L;
	
	private int x, y, z;
	
	public Vec(int x, int y){
		this.x = x;
		this.y = y;
		this.z = 0;
	}
	
	public Vec(int x, int y, int z){
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public int getX(){ return x; }
	public int getY(){ return y; }
	public int getZ(){ return z; }
	
	public Vec copy(){ return new Vec(this.x, this.y, this.z); }
	
	public Vec add(Vec vec){
		return new Vec(this.x+vec.x, this.y+vec.y, this.z+vec.z);
	}
	
	public Vec sub(Vec vec){
		return new Vec(this.x-vec.x, this.y-vec.y, this.z-vec.z);
	}
	
	public Vec mult(double s){
		return new Vec((int)Math.round(this.x*s), (int)Math.round(this.y*s), (int)Math.round(this.z*s));
	}
	
	public Vec div(double s){
		return new Vec((int)Math.round(this.x/s), (int)Math.round(this.y/s), (int)Math.round(this.z/s));
	}
	
	public int dotP(Vec vec){
		return this.x*vec.x+this.y*vec.y+this.z*vec.z;
	}
	
	public int norm(){
		return (int) Math.sqrt(this.dotP(this));
	}
	
	public boolean equals(Vec vec){
		if(vec.x==this.x && vec.y==this.y && vec.z==this.z) return true;
		else return false;
	}
	
	public String toString(){
		if(z==0) return "["+this.x+" "+this.y+"]";
		else return "["+this.x+" "+this.y+" "+this.z+"]";
	}
			
	public Point toPoint(){
		return new Point((int)this.x, (int)this.y);
	}
	
	public Rectangle toRect(Vec vec){
		return new Rectangle(this.x, this.y, vec.x, vec.y);
	}
	
	public static Vec parseVec(String s) throws NumberFormatException{
		s = s.replace("[","").replace("]","");
		String[] sa = s.split(" ");
		if(sa.length<2) sa = s.split(",");
		if(sa.length<2) sa = s.split("x");
		if(sa.length<2) sa = s.split("/");
		for(String si : sa) si = si.trim();
		if(sa.length==2)
			return new Vec(Integer.parseInt(sa[0]),Integer.parseInt(sa[1]));
		else if(sa.length==3)
			return new Vec(Integer.parseInt(sa[0]),Integer.parseInt(sa[1]),Integer.parseInt(sa[2]));
		else throw new NumberFormatException();
	}
}