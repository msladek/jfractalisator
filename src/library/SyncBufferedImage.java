package library;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Stack;



public class SyncBufferedImage{
	
	private int imageType;
	
	private BufferedImage img;
	private int[][] es;
	
	private ArrayList<Vec> al;
	private Stack<Vec> s;
	
	private Object lock;
	
	private boolean waked;
	
	public SyncBufferedImage(Vec v, int imageType){
		this.img = new BufferedImage(v.getX(), v.getY(), this.imageType=imageType);
		this.es = new int[v.getX()][v.getY()];
		this.al = new ArrayList<Vec>();
		this.s = new Stack<Vec>();
		this.lock = new Object();
		this.waked = false;
		//do not reset due to notifyAll()!
		
		for(int i=1;i>=0;i--)
			for(int x=v.getX()-1;x>=0;x--)
				for(int y=v.getY()-1;y>=0;y--)
					al.add(new Vec(x,y,i));
	}
	
	public BufferedImage toBufferedImage(){ return img; }
	
	public Vec getSize(){ return new Vec(img.getWidth(), img.getHeight()); }
	public void setRGB(int x, int y, int rgb){ img.setRGB(x, y, rgb); }
	public void setES(int x, int y, int es){ this.es[x][y] = es; }
	
	public int getES(int x, int y){ return this.es[x][y]; }
			
	public synchronized Vec getNext(){
		while(true)
			if(s.isEmpty()){
				if(!waked)
					synchronized(lock){
						//System.out.println("Image is rendered, master!");
						waked = true;
						lock.notify();
					}
				try{
					//System.out.println("You better wait a sec, thread!");
					wait();
				} catch(InterruptedException e){ return null; }
			}
			else
				return s.pop();
	}
	
	public synchronized void setSize(Vec v){
		this.img = new BufferedImage(v.getX(), v.getY(), imageType);
		this.es = new int[v.getX()][v.getY()];
		this.al.clear();
		for(int i=1;i>=0;i--)
			for(int x=v.getX()-1;x>=0;x--)
				for(int y=v.getY()-1;y>=0;y--)
					al.add(new Vec(x,y,i));
	}
		
	public synchronized void reset(){
		s.clear();
		for(Vec v : al)
			s.push(v);
		//System.out.println("Get back to work, threads!");
		notifyAll();
		System.gc();
	}
		
	public boolean waiting(){
		synchronized(lock){
			if(!s.isEmpty())
				//System.out.println("I beg you for patience, master!");
				try{
					waked = false;
					lock.wait();
					return true;
				} catch(InterruptedException e){
					return false;
				}
			else
				return false;
		}
	}
}
