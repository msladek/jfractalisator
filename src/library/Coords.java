package library;

import java.io.Serializable;

public class Coords extends Object implements Serializable{
	
	private static final long serialVersionUID = 2802201206481L;
	
	private Complex c_in;
	private double c_step, c_step_z;
	
	public Coords(Vec size){
		this.c_in =  new Complex(-2.2,-2.15);
		this.c_step = 0.0045/size.getY()*960;
		this.c_step_z = 1;
	}
	
	public Coords(Complex c_in, double c_step, double c_step_z){
		this.c_in = c_in;
		this.c_step = c_step;
		this.c_step_z = c_step_z;
	}
	
	public String toString(){
		return c_in.round(1000)+" "+(int)c_step_z+"x";
	}
	
	public Coords copy(){
		return new Coords(c_in.copy(), c_step, c_step_z);
	}
	
	public Complex getC(){ return this.c_in; }
	public double getZoom(){ return this.c_step_z; }
			
	void newSize(Vec old_size, Vec new_size){
		this.c_step = this.c_step*old_size.getY()/new_size.getY();
	}
	
	public double step(){
		return c_step / c_step_z;
	}
	
	public Complex transform(Vec v){
		return new Complex(
				c_in.getReal()+step()*v.getX() ,
				c_in.getImag()+step()*v.getY()
		);
	}
	
	private Complex retransform(Complex c, Vec v){
		return new Complex(
				c.getReal()-step()*v.getX() ,
				c.getImag()-step()*v.getY()
		);
	}
	
	public void zoom(Vec size, Vec click, int zoomvar, int zoom){
		Complex c = transform(click);
		
		if(zoomvar==0) c_step_z *= zoom;
		else if(zoomvar==1) c_step_z /= zoom;
		
		c_in = retransform(c, size.div(2));
	}
}