package library;

import java.io.Serializable;


public class Data extends Object implements Serializable{
	
	private static final long serialVersionUID = 2802201205261L;

	public Coords coords;
	public Fractal fractal;
	public Frac power;
	public boolean julia_s;
	public Complex julia;
	public int max_iter;
	private int es_min, es_max;
	public ColorPattern cp;
	
	public Data(Coords coords, Fractal fractal, Frac power, boolean julia_s, Complex julia,  int max_iter, ColorPattern cp){
		this.coords = coords;
		this.fractal = fractal;
		this.power = power;
		this.julia_s = julia_s;
		this.julia = julia;
		this.max_iter = max_iter;
		this.cp = cp;
	}
	
	public String toString(){
		return fractal+" "+cp.getColor()+" "+cp.getType()+" "+power.toString().replace("/",",")+" "+max_iter+" "+coords+" "+(julia_s?julia:"");
	}
	
	public Data copy(){
		return new Data(coords.copy(), fractal, power.copy(), julia_s, julia.copy(),  max_iter, cp.copy());
	}
	
	public int getESmin(){ return this.es_max<this.es_min ? this.es_max : this.es_min; }
	public int getESmax(){ return this.es_max; }
	
	public int getRGB(int es){ return this.cp.getRGB(es, this.es_min, this.es_max, this.max_iter, this.power); }
	
	public boolean needCoordsReset(Data data){
		if(julia_s!=data.julia_s) return true;
		else if(!fractal.equals(data.fractal)) return true;
		else if(!power.equals(data.power)) return true;
		else if(!julia.equals(data.julia)) return true;
		else return false;
	}
	
	public void setESMinMax(int es){
		if(es>this.es_max)
			this.es_max = es;
		if(es<this.es_min)
			this.es_min = es;
	}
}