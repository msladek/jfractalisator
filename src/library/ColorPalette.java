package library;

public class ColorPalette {
	
	private double u;
	public static double u_rgb[] = new double[]{0.25,0.5,0.75};
	
	public ColorPalette(double u){
		u = u%1;
		this.u = u<0 ? 1-u : u;
	}
	
	public ColorPalette setU(double u){
		this.u = u;
		return this;
	}
	
	public int[] toRGB(){
		int[] rgb = new int[3];
		
		int val,
			wh = (wh=(int)(255-Math.abs(u>0.5?1-u:u)/0.125*255))<0 ? 0 : wh;
		for(int i=0;i<rgb.length;i++)
			rgb[i] = wh>0 ? wh : (val=(int)(255-Math.abs(u-u_rgb[i])/0.125*255))<0 ? 0 : val;		
		return rgb;
	}

}
