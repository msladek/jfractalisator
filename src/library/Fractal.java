package library;

public enum Fractal {
	Mandelbrot,
	BurningShip,
	Mandelbar,
	Pokorny,
	BurningPokorny,
	Test,
	PhoenixDEC
}
