package library;

public enum ColorType{
	new_linear,
	new_root,
	linear_halfpeak,
	linear_tricolore,
	linear,
	border,
	border_white,
	logarithmic,
	exponential
}
